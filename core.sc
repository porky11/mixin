using import utils.va
using import utils.functional
using import utils.utils

let mixin =
    typename "mixin"

let empty-mixin =
    typename "empty-mixin"
        super = mixin
        storage = (tuple)

typefn empty-mixin 'empty? (self)
    true

typefn mixin 'empty? (self)
    false

typefn& empty-mixin 'empty? (self)
    true

typefn& mixin 'empty? (self)
    false

typefn mixin 'split (self)
    let T =
        typeof self
    return
        extractvalue self 0
        extractvalue self 1

typefn& mixin 'split (self)
    let T =
        typeof& self
    return
        (getelementptr self 0 0) as ref
        (getelementptr self 0 1) as ref

typefn mixin 'get (self)
    let value =
        'split self
    value

typefn mixin 'next (self)
    va@ 1 ('split self)

typefn& mixin 'get (self)
    let value =
        'split self
    value

typefn& mixin 'next (self)
    let result =
        va@ 1 ('split self)
    result

fn mixin-string (self first)
    dump "mixin-string"
    if ('empty? self) ""
    else
        dump "mixin" self
        let component next-mixin =
            'split self
        .. (if first "") (else " ")
            '__repr component
            mixin-string next-mixin false

typefn mixin '__repr (self)
    dump "m repr"
    .. "<"
        mixin-string self true
        ">"

typefn& mixin '__repr (self)
    dump "m repr&"
    .. "<"
        mixin-string self true
        ">"


fn make-mixin-type (components...)
    if (va-empty? components...)
        empty-mixin
    else
        let first rest... = components...
        let submixin =
            make-mixin-type rest...
        let storage-type =
            tuple
                va-map unknownof first submixin
        let mixin-type =
            typename
                template-repr "mixin" (va-reverse components...)
                storage = storage-type
                super = mixin
        set-type-symbol! mixin-type 'ComponentType first
        set-type-symbol! mixin-type 'NextType submixin
        mixin-type


typefn mixin '__typecall (self ...)
    if (self == mixin)
        let components... =
            va-reverse ...
        make-mixin-type components...
    elseif (self == empty-mixin)
        bitcast (tupleof) empty-mixin
    else
        let component-type next-type =
            self.ComponentType ... 
            self.NextType ...
        let result =
            tupleof component-type next-type
        bitcast result self

typefn& mixin '__new (self ...)
    construct ('get self) ...
    construct ('next self) ...

typefn& empty-mixin '__new (self ...)

typefn& mixin '__copy (self other)
    let value next =
        'split self
    let other-value other-next =
        'split other
    copy-construct value other-value
    copy-construct next other-next

typefn& empty-mixin '__copy (self other)

typefn& mixin '__move (self other)
    let value next =
        'split self
    let other-value other-next =
        'split other
    move-construct value other-value
    move-construct next other-next

typefn& empty-mixin '__move (self other)

fn next-call (method self ...)
    method ('next self) ...

typefn mixin '__typeattr (T symbol)
    let method =
        forward-typeattr T.ComponentType symbol
    if (none? value)
        let next =
            forward-typeattr T.NextType symbol
        if (not (none? next))
            fn (self ...)
                next-call next self ...
    else
        fn (self ...)
            value self ...

typefn& mixin '__typeattr (T symbol)
    let value =
        forward-typeattr (ref (pointer T.ComponentType 'mutable)) symbol
    if (none? value)
        let next =
            forward-typeattr (ref (pointer T.NextType 'mutable)) symbol
        if (not (none? next))
            fn (self ...)
                next-call next self ...
    else
        fn (self ...)
            value self ...

typefn empty-mixin '__typeattr (T symbol)

typefn& empty-mixin '__typeattr (T symbol)

typefn mixin '__getattr (self name)
    let result =
        forward-getattr ('get self) name
    if (none? result)
        forward-getattr ('next self) name
    else result

typefn empty-mixin '__getattr (self name)

typefn& mixin '__getattr (self name)
    let result =
        forward-getattr ('get self) name
    if (none? result)
        forward-getattr ('next self) name
    else result

typefn& empty-mixin '__getattr (self name)

locals;


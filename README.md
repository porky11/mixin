This is a library written in scopes for creating types composed out of components.
They will be called mixins.
A component can contain data, expose methods and use methods exposed by other components.

## Types

### Components

The easiest way to create a component is the macro `component`:
```
component component-name
    body...

component component-name : value-type
    body...
```

`component-name`: The name of the component. If it's a symbol, the component will be bound to it, else if it's a string, the component will be returned.

`value-type`: If supplied, this only allows a single elment type for the component. Else you have to create the specialized component type, usable by mixins, using `component-name value-type`

`body...`: In the body additional values can be defined. The current component can also be accessed by `this-component`, but that's normally not needed. There is also a simplyfied way to expose methods, using the local macros `expose` and `expose&`, which will just define methods on these components.

### Mixins

The components themselves cannot be initialized. They can only be used in mixins.
In order to create a mixin type, there is just a single variadic function called `mixin`.

The arguments are component types. All types are required to be specialized on a type.
The mixin will implement all methods exposed by its components.
If a method is implemented by multiple components, only the first implementation will be exposed.

All the methods defined for components will be called on mixins, not on components or value types of components.

## Helper Functions

There are some functions to help creating complex method structures.

### Inside exposed methods

```
'get mixin
```

Returns the value of the current component of `mixin`. It's type is the specified `value-type`

```
'next mixin
```

Returns a mixin which does not contain the first component of `mixin`. If some method is defined on the current component and on some other component, this may be needed.

```
'split mixin
```

Returns two values: `('get mixin) ('next mixin)`

```
exposed? method-name mixin
```

Checks if any of the components of `mixin` exposes a method called `method-name`

```
next-call method-name mixin
```

Calls the method called `method-name` on the next value, which exposes that method.



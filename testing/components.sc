using import ..core
using import ..component
using import utils.utils

using import glm

component pos
    expose& 'translate (self val)
        self.pos += val

component vel
    expose& 'accelerate (self val)
        self.vel += val
    expose& 'update (self)
        let val = self.vel
        valid-call 'update ('next self)
        'translate self val

typefn vec3 'zero (T)
    T 0 0 0

fn zero (T)
    if (none? T.zero)
        T;
    else
        'zero T

component acc
    expose& 'accelerate (self val)
        self.acc += val
    expose& 'update (self)
        next-call 'accelerate self self.acc
        next-call 'translate self self.acc
        valid-call 'update ('next self)
        self.acc =
            zero (typeof& self.acc)

component time : u32
    expose& 'update (self)
        self.time += 1:u32
        valid-call 'update ('next self)

fn mover (T)
    mixin
        pos T
        vel T
        acc T
        time

let const-mover =
    (mover vec3)
        pos = (vec3 0 0 0)
        vel = (vec3 0 0 0)
        acc = (vec3 0 0 0)
        time = 0:u32

print "const mover" const-mover

let test-mover =
    local
        mover vec3
        pos = (vec3 0 0 0)
        vel = (vec3 0 0 0)
        acc = (vec3 0 0 0)
        time = 0:u32

print "test-mover" test-mover

'translate test-mover (vec3 1 0 0)

'accelerate test-mover (vec3 1 2 3)

print
    test-mover.pos

'update test-mover
'update test-mover
'update test-mover
'update test-mover
'update test-mover

print
    test-mover.pos

print test-mover
